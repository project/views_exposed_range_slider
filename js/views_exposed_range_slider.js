(function ($) {
  Drupal.views_exposed_range_slider = Drupal.views_exposed_range_slider || {};

  Drupal.behaviors.views_exposed_range_slider = {
    attach() {
      const sliders = document.querySelectorAll(
        '.view-filters:not(.views_exposed_range_slider-process)',
      );
      sliders.forEach(function (slider) {
        slider.classList.add('views_exposed_range_slider-process');
        Drupal.views_exposed_range_slider.prepareSlider(slider);
      });
    },
  };

  Drupal.views_exposed_range_slider.prepareSlider = function (slider) {
    const filterContainer = slider.querySelector('.row');
    const sliderContainer = document.createElement('div');
    sliderContainer.classList.add('slider-wrapper');
    filterContainer.insertBefore(
      sliderContainer,
      filterContainer.querySelector('div.form-item-min-value'),
    );

    const thumbsize = 14;
    const min = slider.querySelector('.form-item-min-value input');
    const max = slider.querySelector('.form-item-max-value input');

    let rangemin = 9999;
    let rangemax = 0;
    let block = slider;
    let viewsContainer;
    while ((block = block.parentElement)) {
      if (block.matches('.block-views')) {
        viewsContainer = block;
        break;
      }
    }
    if (viewsContainer === undefined) {
      return;
    }
    if (
      viewsContainer.getAttribute('data-min-value') &&
      viewsContainer.getAttribute('data-max-value')
    ) {
      rangemin = parseInt(viewsContainer.getAttribute('data-min-value'));
      rangemax = parseInt(viewsContainer.getAttribute('data-max-value'));
    } else {
      const values = slider.parentNode.querySelectorAll(
        '.views-field-field-net-rental span',
      );
      values.forEach(function (el) {
        const value = parseInt(el.getAttribute('data-value'));
        if (value < rangemin) {
          rangemin = value;
        }
        if (value > rangemax) {
          rangemax = value;
        }
      });
      viewsContainer.setAttribute('data-min-value', rangemin.toString());
      viewsContainer.setAttribute('data-max-value', rangemax.toString());
    }
    const avgvalue = (rangemin + rangemax) / 2;

    /* set range attributes */
    min.setAttribute('type', 'range');
    max.setAttribute('type', 'range');
    min.setAttribute('step', '1');
    max.setAttribute('step', '1');
    min.setAttribute('min', rangemin.toString());
    max.setAttribute('min', rangemin.toString());
    min.setAttribute('max', rangemax.toString());
    max.setAttribute('max', rangemax.toString());

    /* set data-values */
    if (min.getAttribute('value') === '') {
      min.setAttribute('data-value', rangemin.toString());
      max.setAttribute('data-value', rangemax.toString());
    } else {
      min.setAttribute('data-value', min.getAttribute('value'));
      max.setAttribute('data-value', max.getAttribute('value'));
    }

    /* set data vars */
    slider.setAttribute('data-rangemin', rangemin);
    slider.setAttribute('data-rangemax', rangemax);
    slider.setAttribute('data-thumbsize', thumbsize);
    slider.setAttribute('data-rangewidth', slider.offsetWidth);

    /* write labels */
    const lower = document.createElement('span');
    const upper = document.createElement('span');
    lower.classList.add('lower', 'value');
    upper.classList.add('upper', 'value');
    lower.appendChild(document.createTextNode(rangemin.toString()));
    upper.appendChild(document.createTextNode(rangemax.toString()));
    sliderContainer.appendChild(
      filterContainer.querySelector('div.form-item-min-value'),
    );
    sliderContainer.appendChild(
      filterContainer.querySelector('div.form-item-max-value'),
    );
    sliderContainer.append(lower);
    sliderContainer.append(upper);

    /* draw */
    Drupal.views_exposed_range_slider.drawSlider(slider, avgvalue);

    /* events */
    min.addEventListener('input', function () {
      Drupal.views_exposed_range_slider.updateSlider(slider);
    });
    max.addEventListener('input', function () {
      Drupal.views_exposed_range_slider.updateSlider(slider);
    });
  };

  Drupal.views_exposed_range_slider.drawSlider = function (slider, splitvalue) {
    splitvalue = parseInt(splitvalue);

    /* set function vars */
    const min = slider.querySelector('.form-item-min-value input');
    const max = slider.querySelector('.form-item-max-value input');
    const rangemin = parseInt(min.getAttribute('min'));
    const rangemax = parseInt(max.getAttribute('max'));
    const lower = slider.querySelector('.lower');
    const upper = slider.querySelector('.upper');
    const thumbsize = parseInt(slider.getAttribute('data-thumbsize'));
    const rangewidth = parseInt(slider.getAttribute('data-rangewidth'));

    /* set min and max attributes */
    min.setAttribute('max', splitvalue);
    max.setAttribute('min', splitvalue);

    /* set css */
    min.style.width = `${
      thumbsize +
      ((splitvalue - rangemin) / (rangemax - rangemin)) *
        (rangewidth - 2 * thumbsize)
    }px`;
    max.style.width = `${
      thumbsize +
      ((rangemax - splitvalue) / (rangemax - rangemin)) *
        (rangewidth - 2 * thumbsize)
    }px`;
    min.style.left = '0px';
    max.style.left = `${parseInt(min.style.width)}px`;
    min.style.top = `${lower.offsetHeight}px`;
    max.style.top = `${lower.offsetHeight}px`;

    /* correct for 1 off at the end */
    if (max.value > rangemax - 1) {
      max.setAttribute('data-value', rangemax.toString());
    }

    /* write value and labels */
    max.value = max.getAttribute('data-value');
    min.value = min.getAttribute('data-value');
    upper.innerHTML = parseInt(max.getAttribute('data-value')).toLocaleString(
      'de-CH',
    );
    lower.innerHTML = parseInt(min.getAttribute('data-value')).toLocaleString(
      'de-CH',
    );
  };

  Drupal.views_exposed_range_slider.updateSlider = function (slider) {
    /* set function vars */
    const min = slider.querySelector('.form-item-min-value input');
    const max = slider.querySelector('.form-item-max-value input');
    const minvalue = Math.floor(min.value);
    const maxvalue = Math.floor(max.value);

    /* set inactive values before draw */
    min.setAttribute('data-value', minvalue.toString());
    max.setAttribute('data-value', maxvalue.toString());

    const avgvalue = (minvalue + maxvalue) / 2;

    /* draw */
    Drupal.views_exposed_range_slider.drawSlider(slider, avgvalue);
  };
})(jQuery);
