<?php

namespace Drupal\views_exposed_range_slider\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * Display extender plugin for views to render min/max filter as a range slider.
 *
 * @ViewsDisplayExtender(
 *     id = "views_exposed_range_slider",
 *     title = @Translation("Range slider filter"),
 *     help = @Translation("Render a min/max filter as a range slider."),
 *     no_ui = FALSE
 * )
 */
class FilterRangeSlider extends DisplayExtenderPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['enabled'] = ['default' => FALSE];
    $options['filter_min'] = ['default' => ''];
    $options['filter_max'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') !== 'views_exposed_range_slider') {
      return;
    }

    $filters = ['' => $this->t('<None>')];
    /**
     * @var string $filter
     * @var \Drupal\views\Plugin\views\ViewsHandlerInterface $handler
     */
    foreach ($this->displayHandler->getHandlers('filter') as $filter => $handler) {
      $filters[$filter] = $handler->adminLabel();
    }

    $form['#title'] .= $this->t('Range slider filter');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display min/max filter as range slider'),
      '#default_value' => $this->options['enabled'],
    ];
    $form['filter_min'] = [
      '#type' => 'select',
      '#title' => $this->t('Min value'),
      '#options' => $filters,
      '#default_value' => $this->options['filter_min'],
      '#required' => TRUE,
    ];
    $form['filter_max'] = [
      '#type' => 'select',
      '#title' => $this->t('Max value'),
      '#options' => $filters,
      '#default_value' => $this->options['filter_max'],
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->get('section') !== 'views_exposed_range_slider') {
      return;
    }
    $this->options['enabled'] = $form_state->getValue('enabled');
    $this->options['filter_min'] = $form_state->getValue('filter_min');
    $this->options['filter_max'] = $form_state->getValue('filter_max');
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options): void {
    $options['views_exposed_range_slider'] = [
      'category' => 'other',
      'title' => $this->t('Range slider filter'),
      'desc' => $this->t('Display min/max filter as range slider.'),
      'value' => $this->options['enabled'] ? $this->t('Yes') : $this->t('No'),
    ];
  }

  /**
   * Adds libraries and settings to the render array.
   *
   * @param array $variables
   *   The render array.
   */
  public function prepare(array &$variables): void {
    if (!$this->options['enabled']) {
      return;
    }
    $variables['#attached']['library'][] = 'views_exposed_range_slider/generic';
    $variables['#attached']['drupalSettings']['views_exposed_range_slider'] = [
      'filter_min' => $this->options['filter_min'],
      'filter_max' => $this->options['filter_max'],
    ];
  }

}
